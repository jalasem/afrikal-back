const router = require('express').Router()
const {handleErr} = require('../utils.js')

const auth = require('./auth')
const jwt = require('jsonwebtoken')

// Add Routes (no auth needed)
router.use(auth)
router.use((req, res, next) => {
  console.log(JSON.stringify(req.body, null, 2))
  var token = req.headers['x-access-token'] || req.body.token
  if (token) {
    jwt.verify(token, req.app.get('tokenSecret'), (err, decoded) => {
      if (err) throw err
      req.decoded = decoded
      next()
    })
  } else {
    handleErr({}, res, 'You are not authroized!', 403)
  }
})
//  Add routes (auth needed)
router.post('/protected', (req, res) => {
  res.send('You are cool!')
})

module.exports = router
