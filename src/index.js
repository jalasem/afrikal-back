const cluster = require('cluster')
const express = require('express')

const CPUs = require('os')
  .cpus()
  .length

if (cluster.isMaster) {
  console.log(`Master cluster setting up ${CPUs} workers...`)

  for (var i = 0; i < CPUs; i++) {
    cluster.fork()
  }
  cluster.on('online', worker => {
    console.log(`Worker ${worker.process.pid} is online.`)
  })
  cluster.on('exit', (worker, code, signal) => {
    console.log(`Worker ${worker.process.pid} died with code: ${code}, and signal: ${signal}`)

    console.log('Starting a new worker')
    cluster.fork()
  })
} else {
  require('dotenv').config()
  const bodyParser = require('body-parser')
  const helmet = require('helmet')
  const logger = require('morgan')
  const cors = require('cors')
  const mongoose = require('mongoose')

  const app = express()

  const api = require('./api/index')
  const config = require('./config')

  const PORT = process.env.PORT || config.PORT

  // Import API Routes
  mongoose.Promise = global.Promise
  mongoose.connect(process.env.DB_URI || config.db_url, {
    useMongoClient: true,
    reconnectInterval: 1000
  }, (err, db) => {
    if (err) {
      console.log("Couldn't connect to database")
    } else {
      // console.log(`Connected To ${environment} Database`);
      console.log(`Connected To Database`)
    }
  })

  app
    .set('tokenSecret', config.token_secret)
    .use(logger('dev'))
    .use(helmet())
    .disable('x-powered-by')
    .use(cors())
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({extended: true}))

  app.use('/api', api)
  app.use('/', api)

  // app.get('/', (req, res) => {
  //   console.log(`process pid:${process.pid} recvd a request.`)
  //   res.send(`Process:${process.pid} says Hello!`)
  // })

  app.listen(PORT, () => {
    console.log(`Server(${process.pid}) running on PORT:${PORT} is listening to all requests`)
  })
}
